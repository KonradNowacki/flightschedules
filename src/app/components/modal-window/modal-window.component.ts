import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";

@Component({
    selector: "app-modal-window",
    templateUrl: "./modal-window.component.html",
    styleUrls: [
        "./modal-window.component.css",
        "./../../../assets/css/fonts.css"
    ]
})

export class ModalWindowComponent implements OnInit {
    @Input() modalData: {registration};
    @Input() allRegistrations: Array<string>;
    registrationSearch: string;
    showSearchBox = false;
    autocompleteOptions: Array<string> = [];
    @Output() newName = new EventEmitter<any>();
    @Output() closeModal = new EventEmitter<any>();

    constructor() {
    }

    ngOnInit(): void {
        this.registrationSearch = this.modalData.registration;
    }

    closeModalEmit(): void {
        this.closeModal.emit();
    }

    toggleSearchBox(): void {
        if (this.registrationSearch !== undefined) {
            this.showSearchBox = this.registrationSearch.length >= 2;
        }
    }

    resultToSave(newName, row): void {
        this.closeModalEmit();
        this.newName.emit({newName, rowId: row.uniqueId});
    }

    searchName() {
        this.autocompleteOptions = this.allRegistrations.filter(arg => {
            if (arg.toString().toLowerCase().indexOf(this.registrationSearch.toLowerCase()) === 0) {
                return arg;
            }
        });
    }

}
