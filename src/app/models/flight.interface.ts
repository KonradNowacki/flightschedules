export interface IFlight {
    uniqueId: string;
    flightNumber: string;
    scheduledDate: Date;
    originDestination: string;
    registration: string;
}

export interface MockFlightsType {
    uniqueid: number;
    FlightNumber: string;
    DateTime: number;
    "Origin/Destination": string;
    Registration: string;
}
