import {
    Component, EventEmitter,
    Input, Output
} from "@angular/core";
import { IFlight } from "src/app/models/flight.interface";

@Component({
  selector: "data-grid",
  templateUrl: "./data-grid.component.html",
  styleUrls: ["./data-grid.component.css"]
})
export class DataGridComponent {
    @Input() public rows: Array<IFlight>;
    @Output() openModal = new EventEmitter<any>();
    public sampleHeaders = ["flight", "sch. date", "sch.time", "o/d", "registration"];

    openModalWindow($event) {
        this.openModal.emit({row: $event});
    }

}
