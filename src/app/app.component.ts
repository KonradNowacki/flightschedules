import {Component, ViewEncapsulation} from "@angular/core";
import {IFlight} from "src/app/models/flight.interface";
import {MockFlightsType} from "src/app/models/flight.interface";
import {mockFlights} from "./../assets/mock-data/mockFlights";
import {mockRegistrations} from "./../assets/mock-data/mockRegistrations";

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: [
        "../assets/css/reset.css",
        "../assets/css/fonts.css"
    ],
})
export class AppComponent {

    public showModal = false;
    public mockFlights: Array<MockFlightsType> = mockFlights;
    public modalData;
    public allRegistrations: Array<number | string> = mockRegistrations;
    public data: Array<IFlight> = this.prepareData(this.mockFlights);

    prepareData(mockData: MockFlightsType[]) {
        const arr: Array<IFlight> = [];

        for (const flight of mockData) {
            const obj: any = {};
            obj.uniqueId = flight.uniqueid;
            obj.flightNumber = flight.FlightNumber;
            obj.scheduledDate = flight.DateTime;
            obj.originDestination = flight["Origin/Destination"];
            obj.registration = flight.Registration;
            arr.push(obj);
        }
        return arr;
    }

    openModalWindow($event): void {
        this.showModal = true;
        this.modalData = {
            uniqueId: $event.row.uniqueId,
            flightNumber: $event.row.flightNumber,
            scheduledDate: $event.row.scheduledDate,
            registration: $event.row.registration
        };
    }

    closeModal(): void {
        this.showModal = false;
    }

    setNewName($event): void {
        for (const row of this.data) {
            if (row.uniqueId === this.modalData.uniqueId && $event.newName !== undefined) {
                if ($event.newName.length > 0) {
                    row.registration = $event.newName;
                } else {
                    row.registration = "...........";
                }
            }
        }
    }


}
