import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { DataGridModule } from "./components/data-grid/data-grid.module";

import { AppComponent } from "./app.component";
import { ModalWindowComponent } from "./components/modal-window/modal-window.component";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    ModalWindowComponent
  ],
  imports: [
    BrowserModule,
    DataGridModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
